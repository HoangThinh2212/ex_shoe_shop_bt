import "./App.css";
// import Demo_Mini_Redux from "./Demo_Mini_Redux/Demo_Mini_Redux";
// import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
// import DemoState from "./DemoState/DemoState";
// import DemoClass from "./DemoComponent/DemoClass";
// import DemoFunction from "./DemoComponent/DemoFunction";
// import Ex_Layout from "./Ex_Layout/Ex_Layout";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DataBinding from "./DataBinding/DataBinding"; //Học về DataBinding
// import DemoProps from "./DemoProps/DemoProps";
// import Ex_Car from './Ex_Car/Ex_Car';
// import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      {/* Trong đây được gọi là các Instance */}
      {/*  <Ex_Layout />  */}
      {/* <RenderWithMap /> */}
      {/* <DataBinding /> */}
      {/* <DemoProps /> */}
      {/* <DemoState/> */}
      {/* <Ex_Car/> */}
      {/* <ConditionalRendering /> */}
      <Ex_ShoeShop />
      {/* <Demo_Mini_Redux /> */}
      {/* <Ex_ShoeShop_Redux /> */}
    </div>
  );
}

export default App;
