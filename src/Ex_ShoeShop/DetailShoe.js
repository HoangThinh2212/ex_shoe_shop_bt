import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    return (
      <div className="row pt-5">
        <img className="col-4" src={this.props.detail.image} alt="" />
        <div className="col-8">
          {/* Detail */}
          <p>{this.props.detail.name}</p>
          <p>{this.props.detail.price}</p>
          <p>{this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}
