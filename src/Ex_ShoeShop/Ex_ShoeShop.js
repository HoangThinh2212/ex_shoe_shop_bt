import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      //th2: sp chưa có trong giỏ hàng -> tạo mới và push
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      //th1: sp đã có trong giỏ hàng -> tăng key number ==> tìm kiếm trong mảng cart biến shoe
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleChangeQty = (data, value) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === data;
    });
    if (cloneCart[index].number === 1 && value === -1) {
      cloneCart.splice(index, 1);
    } else {
      cloneCart[index].number += value;
    }
    this.setState({ cart: cloneCart });
  };

  // Viết 1 hàm render các item -> viết thêm 1 hàm ListShoe.js
  render() {
    return (
      <div className="container">
        {/* cart */}
        <Cart cart={this.state.cart} handleChangeQty={this.handleChangeQty} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        {/* Xem chi tiết */}
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
