import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    // return của list
    return this.props.shoeArr.map((item) => {
      // return của map --- là return 1 array
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCart}
          handleViewDetail={this.props.handleChangeDetail}
          data={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
